import { Person } from "./person";
import { SalesTerritory } from "./salesTerritory";
import { Store } from "./store";

export interface Customer {
    customerId: number;
    personId: number | null;
    storeId: number | null;
    territoryId: number | null;
    accountNumber: string;
    rowguid: string;
    modifiedDate: Date;
    person: Person;
    store: Store;
    territory: SalesTerritory;
}