export interface Person {
    businessEntityId: number;
    fullName: string;
    firstName: string;
    middleName: string;
    lastName: string;
}