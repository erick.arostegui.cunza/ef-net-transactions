export interface PagedResult {
    pageNumber: number;
    pageSize: number;
    totalPages: number;
    totalItems: number;
}