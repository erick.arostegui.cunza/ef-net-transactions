export interface SalesTerritory {
    territoryId: number;
    name: string;
}