import { Component } from '@angular/core';
import {merge, Observable, of as observableOf, Subject} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {ViewChild, AfterViewInit} from '@angular/core';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import {MatSort, MatSortModule, SortDirection} from '@angular/material/sort';
import { customersService } from '../services/customers/customers.service';
import { Customer } from '../models/customer';
import { MatDialog } from '@angular/material/dialog';
import { CustomerComponent } from '../customer/customer.component';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements AfterViewInit {
  displayedColumns: string[] = ['accountNumber', 'customerName', 'storeName', 'territoryName','modifiedDate'];
  customers: Customer[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  loadData$ = new Subject<void>();

  constructor(
    private customersService: customersService,
    public dialog: MatDialog){
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page, this.loadData$)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;          
          return this.customersService.getAll(
            this.sort.active,
            this.sort.direction,
            this.paginator.pageIndex,
            this.paginator.pageSize
          ).pipe(catchError(() => observableOf(null)));
        }),
        map(data => {          
          this.isLoadingResults = false;
          this.isRateLimitReached = data === null;

          if (data === null) {
            return [];
          }          
          this.resultsLength = data.pagedResult.totalItems;
          return data.items;
        }),
      )
      .subscribe(data => (this.customers = data));
  }

  public btnClickAddCustomer(e:any){
    const dialogRef = this.dialog.open(CustomerComponent,{
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadData$.next();
    });    
  }
}
