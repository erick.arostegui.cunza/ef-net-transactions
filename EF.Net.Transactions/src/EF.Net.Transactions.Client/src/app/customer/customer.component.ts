import { Component, OnInit } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Person } from '../models/person';
import { SalesTerritory } from '../models/salesTerritory';
import { Store } from '../models/store';
import { customersService } from '../services/customers/customers.service';
import { PeopleService } from '../services/people/people.service';
import { SalesTerritoriesService } from '../services/salesTerritories/sales-territories.service';
import { StoresService } from '../services/stores/stores.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  people: Person[] = [];
  stores: Store[] = [];
  salesTerritories: SalesTerritory[] = [];

  selectedPerson: number = 0;
  selectedStore: number = 0;
  selectedSalesTerritory: number = 0;

  constructor(
    private customersService: customersService,
    private peopleService: PeopleService,
    private storesService: StoresService,
    private salesTerritoriesService: SalesTerritoriesService,
    private dialogRef: MatDialogRef<CustomerComponent>) {
  }

  ngOnInit(): void {
    this.peopleService.getAll("firstName", "asc", 0, 30).subscribe((response) => {
      this.people = response.items;
    });
    this.storesService.getAll("storeName", "asc", 0, 30).subscribe((response) => {
      this.stores = response.items;
    });
    this.salesTerritoriesService.getAll("territoryName", "asc", 0, 30).subscribe((response) => {
      this.salesTerritories = response.items;
    });
  }

  public btnSaveOnClick(e: any) {
    if (this.selectedPerson != 0 && this.selectedStore != 0 && this.selectedSalesTerritory != 0) {
      this.customersService.addCustomer({ personId: this.selectedPerson, storeId: this.selectedStore, territoryId: this.selectedSalesTerritory }).subscribe(customerId=>{
        this.dialogRef.close();
      });      
    }
  }

  public btnCancelOnClick(e: any) {
    this.dialogRef.close();
  }
}
