import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SortDirection } from '@angular/material/sort';
import { catchError, Observable, throwError } from 'rxjs';
import { GetAllPeopleResponse } from './getAllPeopleResponse';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  private peopleUrl = "https://localhost:7032/api/people"
  constructor(
    private http: HttpClient,
  ) { }

  public getAll(sort: string, order: SortDirection, pageNumber: number, pageSize: number): Observable<GetAllPeopleResponse> {
    const getAllUrl = `${this.peopleUrl}?sort=${sort}&order=${order}&pageNumber=${pageNumber + 1}&pageSize=${pageSize}`
    return this.http.get<GetAllPeopleResponse>(getAllUrl)
      .pipe(catchError(this.handleError));    
  }

  private handleError(error: HttpErrorResponse) {    
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return throwError(()=>errMessage);
    }
    return throwError(()=>error || 'Server error');
  }
}
