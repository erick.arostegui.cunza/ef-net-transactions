import { PagedResult } from "src/app/models/pagedResult";
import { Person } from "src/app/models/person";

export interface GetAllPeopleResponse{
    items: Person[];
    pagedResult: PagedResult;
}