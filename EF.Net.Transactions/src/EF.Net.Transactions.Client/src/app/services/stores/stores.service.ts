import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { SortDirection } from '@angular/material/sort';
import { GetAllStoresResponse } from './getAllStoresResponse';

@Injectable({
  providedIn: 'root'
})
export class StoresService {

  private storesUrl = "https://localhost:7032/api/stores"
  constructor(
    private http: HttpClient,
  ) { }

  public getAll(sort: string, order: SortDirection, pageNumber: number, pageSize: number): Observable<GetAllStoresResponse> {
    const getAllUrl = `${this.storesUrl}?sort=${sort}&order=${order}&pageNumber=${pageNumber + 1}&pageSize=${pageSize}`
    return this.http.get<GetAllStoresResponse>(getAllUrl)
      .pipe(catchError(this.handleError));    
  }

  private handleError(error: HttpErrorResponse) {    
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return throwError(()=>errMessage);
    }
    return throwError(()=>error || 'Server error');
  }
}
