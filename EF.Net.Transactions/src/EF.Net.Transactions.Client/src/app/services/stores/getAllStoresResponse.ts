import { PagedResult } from "src/app/models/pagedResult";
import { Store } from "src/app/models/store";

export interface GetAllStoresResponse{
    items: Store[];
    pagedResult: PagedResult;
}