import { PagedResult } from "src/app/models/pagedResult";
import { SalesTerritory } from "src/app/models/salesTerritory";

export interface GetAllSalesTerritoriesResponse{
    items: SalesTerritory[];
    pagedResult: PagedResult;
}