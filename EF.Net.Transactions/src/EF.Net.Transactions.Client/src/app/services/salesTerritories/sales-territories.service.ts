import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SortDirection } from '@angular/material/sort';
import { catchError, Observable, throwError } from 'rxjs';
import { GetAllSalesTerritoriesResponse } from './getAllSalesTerritoriesResponse';

@Injectable({
  providedIn: 'root'
})
export class SalesTerritoriesService {

  private salesTerritoriesUrl = "https://localhost:7032/api/salesTerritories"
  constructor(
    private http: HttpClient,
  ) { }

  public getAll(sort: string, order: SortDirection, pageNumber: number, pageSize: number): Observable<GetAllSalesTerritoriesResponse> {
    const getAllUrl = `${this.salesTerritoriesUrl}?sort=${sort}&order=${order}&pageNumber=${pageNumber + 1}&pageSize=${pageSize}`
    return this.http.get<GetAllSalesTerritoriesResponse>(getAllUrl)
      .pipe(catchError(this.handleError));    
  }

  private handleError(error: HttpErrorResponse) {    
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return throwError(()=>errMessage);
    }
    return throwError(()=>error || 'Server error');
  }
}
