export interface AddCustomerRequest {
    personId: number;
    storeId: number;
    territoryId: number;
}