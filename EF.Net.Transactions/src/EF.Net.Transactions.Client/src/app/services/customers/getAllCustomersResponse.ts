import { Customer } from "src/app/models/customer";
import { PagedResult } from "src/app/models/pagedResult";

export interface GetAllCustomersResponse{
    items: Customer[];
    pagedResult: PagedResult;
}