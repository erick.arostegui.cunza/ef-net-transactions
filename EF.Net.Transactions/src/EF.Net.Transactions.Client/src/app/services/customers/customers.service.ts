import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import {SortDirection} from '@angular/material/sort';
import { GetAllCustomersResponse } from './getAllCustomersResponse';
import { AddCustomerRequest } from './addCustomerRequest';

@Injectable({
  providedIn: 'root'
})
export class customersService {

  private customersUrl = "https://localhost:7032/api/customers"
  constructor(
    private http: HttpClient,
  ) { }

  public getAll(sort: string, order: SortDirection, pageNumber: number, pageSize: number): Observable<GetAllCustomersResponse> {
    const getAllUrl = `${this.customersUrl}?sort=${sort}&order=${order}&pageNumber=${pageNumber + 1}&pageSize=${pageSize}`
    return this.http.get<GetAllCustomersResponse>(getAllUrl)
      .pipe(catchError(this.handleError));    
  }

  public addCustomer(request: AddCustomerRequest):Observable<number>{
    const addUrl = `${this.customersUrl}`
    return this.http.post<number>(addUrl,request)
      .pipe(catchError(this.handleError));    
  }

  private handleError(error: HttpErrorResponse) {    
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return throwError(()=>errMessage);
    }
    return throwError(()=>error || 'Server error');
  }
}
