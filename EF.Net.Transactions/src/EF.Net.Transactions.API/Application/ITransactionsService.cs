﻿using EF.Net.Transactions.API.Infrastructure.Http.Customers;
using EF.Net.Transactions.API.Infrastructure.Http.People;
using EF.Net.Transactions.API.Infrastructure.Http.SalesTerritories;
using EF.Net.Transactions.API.Infrastructure.Http.Stores;

namespace EF.Net.Transactions.API.Application
{
    public interface ITransactionsService
    {
        int AddCustomer(AddCustomerRequest request);
        GetAllCustomersResponse GetCustomers(string sort, string order, int pageNumber, int pageSize);
        GetAllPeopleResponse GetPeople(string sort, string order, int pageNumber, int pageSize);
        GetAllSalesTerritoriesResponse GetSalesTerritories(string sort, string order, int pageNumber, int pageSize);
        GetAllStoresResponse GetStores(string sort, string order, int pageNumber, int pageSize);
    }
}