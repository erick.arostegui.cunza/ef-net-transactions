﻿using EF.Net.Transactions.API.Infrastructure.Data.Models;

namespace EF.Net.Transactions.API.Application.Dto
{
    public class CustomerDto
    {
        public int CustomerId { get; set; }
        public int? PersonId { get; set; }
        public int? StoreId { get; set; }
        public int? TerritoryId { get; set; }
        public string AccountNumber { get; set; }
        public Guid Rowguid { get; set; }
        public DateTime ModifiedDate { get; set; }
        public virtual PersonDto Person { get; set; }
        public virtual StoreDto Store { get; set; }
        public virtual SalesTerritoryDto Territory { get; set; }
    }
}
