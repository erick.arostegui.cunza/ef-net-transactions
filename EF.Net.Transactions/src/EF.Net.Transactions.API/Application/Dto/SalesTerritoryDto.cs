﻿namespace EF.Net.Transactions.API.Application.Dto
{
    public class SalesTerritoryDto
    {
        public int TerritoryId { get; set; }
        public string Name { get; set; }
    }
}
