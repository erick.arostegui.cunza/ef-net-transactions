﻿namespace EF.Net.Transactions.API.Application.Dto
{
    public class PersonDto
    {
        public int BusinessEntityId { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    }
}
