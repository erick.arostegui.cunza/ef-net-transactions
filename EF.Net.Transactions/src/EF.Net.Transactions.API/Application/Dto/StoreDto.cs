﻿namespace EF.Net.Transactions.API.Application.Dto
{
    public class StoreDto
    {
        public int BusinessEntityId { get; set; }
        public string Name { get; set; }

    }
}
