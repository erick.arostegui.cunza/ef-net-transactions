﻿using EF.Net.Transactions.API.Application.Dto;
using EF.Net.Transactions.API.Infrastructure.Data.Contexts;
using System.Linq.Dynamic.Core;
using System.Linq;
using EF.Net.Transactions.API.Infrastructure.Http.People;
using EF.Net.Transactions.API.Infrastructure.Http.Stores;
using EF.Net.Transactions.API.Infrastructure.Http.SalesTerritories;
using EF.Net.Transactions.API.Infrastructure.Http.Customers;
using EF.Net.Transactions.API.Infrastructure.Data.Models;

namespace EF.Net.Transactions.API.Application
{
    public class TransactionsService : ITransactionsService
    {
        private readonly AdventureWorksContext _context;
        public TransactionsService(AdventureWorksContext context)
        {
            _context = context;
        }
        public GetAllPeopleResponse GetPeople(string sort, string order, int pageNumber, int pageSize)
        {
            var query = _context.People.AsQueryable();

            if (!string.IsNullOrEmpty(sort) && !string.IsNullOrEmpty(order))
            {
                if (sort == "firstName")
                {
                    query = order.ToLower() == "desc" ?
                            query.OrderByDescending(p => p.FirstName) :
                            query.OrderBy(p => p.FirstName);
                }
            }

            var projectionQuery = from p in query
                                  select new PersonDto
                                  {
                                      BusinessEntityId = p.BusinessEntityId,
                                      FirstName = p.FirstName,
                                      MiddleName = p.MiddleName,
                                      LastName = p.LastName,
                                      FullName = $"{p.FirstName} {p.MiddleName} {p.LastName}"
                                  };

            int totalItems = query.Count();
            int totalPages = (int)Math.Ceiling(totalItems / (double)pageSize);
            List<PersonDto> people = projectionQuery.Skip((pageNumber - 1) * pageSize)
                                                         .Take(pageSize)
                                                         .ToList();

            GetAllPeopleResponse response = new()
            {
                Items = people,
                PagedResult = new Infrastructure.Http.PagedResult
                {
                    PageNumber = pageNumber,
                    TotalItems = totalItems,
                    TotalPages = totalPages,
                    PageSize = pageSize
                }
            };

            return response;
        }
        public GetAllStoresResponse GetStores(string sort, string order, int pageNumber, int pageSize)
        {
            var query = _context.Stores.AsQueryable();

            if (!string.IsNullOrEmpty(sort) && !string.IsNullOrEmpty(order))
            {
                if (sort == "storeName")
                {
                    query = order.ToLower() == "desc" ?
                            query.OrderByDescending(s => s.Name) :
                            query.OrderBy(s => s.Name);
                }
            }

            var projectionQuery = from s in query
                                  select new StoreDto
                                  {
                                      BusinessEntityId = s.BusinessEntityId,
                                      Name = s.Name
                                  };

            int totalItems = query.Count();
            int totalPages = (int)Math.Ceiling(totalItems / (double)pageSize);
            List<StoreDto> stores = projectionQuery.Skip((pageNumber - 1) * pageSize)
                                                         .Take(pageSize)
                                                         .ToList();

            GetAllStoresResponse response = new()
            {
                Items = stores,
                PagedResult = new Infrastructure.Http.PagedResult
                {
                    PageNumber = pageNumber,
                    TotalItems = totalItems,
                    TotalPages = totalPages,
                    PageSize = pageSize
                }
            };

            return response;
        }
        public GetAllSalesTerritoriesResponse GetSalesTerritories(string sort, string order, int pageNumber, int pageSize)
        {
            var query = _context.SalesTerritories.AsQueryable();

            if (!string.IsNullOrEmpty(sort) && !string.IsNullOrEmpty(order))
            {
                if (sort == "territoryName")
                {
                    query = order.ToLower() == "desc" ?
                            query.OrderByDescending(t => t.Name) :
                            query.OrderBy(t => t.Name);
                }
            }

            var projectionQuery = from s in query
                                  select new SalesTerritoryDto
                                  {
                                      TerritoryId = s.TerritoryId,
                                      Name = s.Name
                                  };

            int totalItems = query.Count();
            int totalPages = (int)Math.Ceiling(totalItems / (double)pageSize);
            List<SalesTerritoryDto> salesTerritories = projectionQuery.Skip((pageNumber - 1) * pageSize)
                                                         .Take(pageSize)
                                                         .ToList();

            GetAllSalesTerritoriesResponse response = new()
            {
                Items = salesTerritories,
                PagedResult = new Infrastructure.Http.PagedResult
                {
                    PageNumber = pageNumber,
                    TotalItems = totalItems,
                    TotalPages = totalPages,
                    PageSize = pageSize
                }
            };

            return response;
        }
        public GetAllCustomersResponse GetCustomers(string sort, string order, int pageNumber, int pageSize)
        {
            var query = _context.Customers.Where(c => c.PersonId != null && c.StoreId != null);

            if (!string.IsNullOrEmpty(sort) && !string.IsNullOrEmpty(order))
            {
                if (sort == "customerName")
                {
                    query = order.ToLower() == "desc" ?
                            query.OrderByDescending(c => c.Person.FirstName) :
                            query.OrderBy(c => c.Person.FirstName);
                }
                if (sort == "modifiedDate")
                {
                    query = order.ToLower() == "desc" ?
                            query.OrderByDescending(c => c.ModifiedDate) :
                            query.OrderBy(c => c.ModifiedDate);
                }
            }

            var projectionQuery = from c in query
                                  select new CustomerDto
                                  {
                                      Rowguid = c.Rowguid,
                                      AccountNumber = c.AccountNumber,
                                      CustomerId = c.CustomerId,
                                      ModifiedDate = c.ModifiedDate,
                                      PersonId = c.PersonId,
                                      Person = !c.PersonId.HasValue ? null : new PersonDto
                                      {
                                          BusinessEntityId = c.Person.BusinessEntityId,
                                          FirstName = c.Person.FirstName,
                                          MiddleName = c.Person.MiddleName,
                                          LastName = c.Person.LastName,
                                          FullName = $"{c.Person.FirstName} {c.Person.MiddleName} {c.Person.LastName}"
                                      },
                                      StoreId = c.StoreId,
                                      Store = !c.StoreId.HasValue ? null : new StoreDto
                                      {
                                          BusinessEntityId = c.Store.BusinessEntityId,
                                          Name = c.Store.Name
                                      },
                                      TerritoryId = c.TerritoryId,
                                      Territory = new SalesTerritoryDto
                                      {
                                          TerritoryId = c.Territory.TerritoryId,
                                          Name = c.Territory.Name
                                      }
                                  };

            int totalItems = query.Count();
            int totalPages = (int)Math.Ceiling(totalItems / (double)pageSize);
            List<CustomerDto> customers = projectionQuery.Skip((pageNumber - 1) * pageSize)
                                                         .Take(pageSize)
                                                         .ToList();

            GetAllCustomersResponse response = new()
            {
                Items = customers,
                PagedResult = new Infrastructure.Http.PagedResult
                {
                    PageNumber = pageNumber,
                    TotalItems = totalItems,
                    TotalPages = totalPages,
                    PageSize = pageSize
                }
            };

            return response;
        }

        public int AddCustomer(AddCustomerRequest request)
        {
            Customer customer = new Customer
            {
                PersonId = request.PersonId,
                StoreId = request.StoreId,
                TerritoryId = request.TerritoryId
            };

            _context.Customers.Add(customer);
            _context.SaveChanges();

            return customer.CustomerId;
        }

    }
}
