using EF.Net.Transactions.API.Application;
using EF.Net.Transactions.API.Infrastructure.Data.Contexts;
using EF.Net.Transactions.API.Infrastructure.Http.Customers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

var builder = WebApplication.CreateBuilder(args);

string adventureWorksConnection = builder.Configuration.GetConnectionString("AdventureWorksConnection");
builder.Services.AddDbContext<AdventureWorksContext>(opt => opt.UseSqlServer(adventureWorksConnection));
builder.Services.AddScoped<ITransactionsService, TransactionsService>();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAuthorization();
builder.Services.AddCors(opt =>
{
    opt.AddPolicy("AllowAllCORS", policy =>
    {
        policy
        .AllowAnyHeader()
        .AllowAnyMethod()
        .AllowAnyOrigin();
    });
});


var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.UseCors("AllowAllCORS");

app.MapGet("/api/Customers", (ITransactionsService transactionsService, HttpContext httpContext,[FromQuery] string? sort, [FromQuery] string? order, [FromQuery] int? pageNumber, [FromQuery] int? pageSize) =>
{
    sort = sort.IsNullOrEmpty() ? "customerName" : sort;
    order = order.IsNullOrEmpty() ? "desc" : order;
    pageNumber = pageNumber.HasValue ? pageNumber : 1 ;
    pageSize = pageSize.HasValue ? pageSize : 30;

    return Results.Ok(transactionsService.GetCustomers(sort, order, pageNumber.Value,pageSize.Value));
});
app.MapPost("/api/Customers", (ITransactionsService transactionsService, [FromBody] AddCustomerRequest request) =>
{
    return Results.Ok(transactionsService.AddCustomer(request));
});

app.MapGet("/api/people", (ITransactionsService transactionsService, HttpContext httpContext, [FromQuery] string? sort, [FromQuery] string? order, [FromQuery] int? pageNumber, [FromQuery] int? pageSize) =>
{
    sort = sort.IsNullOrEmpty() ? "firstName" : sort;
    order = order.IsNullOrEmpty() ? "desc" : order;
    pageNumber = pageNumber.HasValue ? pageNumber : 1;
    pageSize = pageSize.HasValue ? pageSize : 30;
    return Results.Ok(transactionsService.GetPeople(sort, order, pageNumber.Value, pageSize.Value));
});

app.MapGet("/api/stores", (ITransactionsService transactionsService, HttpContext httpContext, [FromQuery] string? sort, [FromQuery] string? order, [FromQuery] int? pageNumber, [FromQuery] int? pageSize) =>
{
    sort = sort.IsNullOrEmpty() ? "storeName" : sort;
    order = order.IsNullOrEmpty() ? "desc" : order;
    pageNumber = pageNumber.HasValue ? pageNumber : 1;
    pageSize = pageSize.HasValue ? pageSize : 30;
    return Results.Ok(transactionsService.GetStores(sort, order, pageNumber.Value, pageSize.Value));
});
app.MapGet("/api/salesTerritories", (ITransactionsService transactionsService, HttpContext httpContext, [FromQuery] string? sort, [FromQuery] string? order, [FromQuery] int? pageNumber, [FromQuery] int? pageSize) =>
{
    sort = sort.IsNullOrEmpty() ? "territoryName" : sort;
    order = order.IsNullOrEmpty() ? "desc" : order;
    pageNumber = pageNumber.HasValue ? pageNumber : 1;
    pageSize = pageSize.HasValue ? pageSize : 30;
    return Results.Ok(transactionsService.GetSalesTerritories(sort, order, pageNumber.Value, pageSize.Value));
});


app.Run();
