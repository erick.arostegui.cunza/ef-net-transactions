﻿using EF.Net.Transactions.API.Application.Dto;

namespace EF.Net.Transactions.API.Infrastructure.Http.Stores
{
    public class GetAllStoresResponse
    {
        public List<StoreDto> Items { get; set; }
        public PagedResult PagedResult { get; set; }
    }
}
