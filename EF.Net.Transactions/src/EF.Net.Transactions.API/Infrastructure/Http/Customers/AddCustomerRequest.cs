﻿namespace EF.Net.Transactions.API.Infrastructure.Http.Customers
{
    public class AddCustomerRequest
    {
        public int PersonId { get; set; }
        public int StoreId { get; set; }
        public int TerritoryId { get; set; }
    }
}
