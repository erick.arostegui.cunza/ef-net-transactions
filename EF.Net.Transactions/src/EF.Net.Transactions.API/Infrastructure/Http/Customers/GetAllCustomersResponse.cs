﻿using EF.Net.Transactions.API.Application.Dto;

namespace EF.Net.Transactions.API.Infrastructure.Http.Customers
{
    public class GetAllCustomersResponse
    {
        public List<CustomerDto> Items { get; set; }
        public PagedResult PagedResult { get; set; }
    }
}
