﻿using EF.Net.Transactions.API.Application.Dto;

namespace EF.Net.Transactions.API.Infrastructure.Http.People
{
    public class GetAllPeopleResponse
    {
        public List<PersonDto> Items { get; set; }
        public PagedResult PagedResult { get; set; }
    }
}
