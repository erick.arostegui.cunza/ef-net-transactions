﻿using EF.Net.Transactions.API.Application.Dto;

namespace EF.Net.Transactions.API.Infrastructure.Http.SalesTerritories
{
    public class GetAllSalesTerritoriesResponse
    {
        public List<SalesTerritoryDto> Items { get; set; }
        public PagedResult PagedResult { get; set; }
    }
}
