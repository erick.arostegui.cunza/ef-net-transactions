﻿namespace EF.Net.Transactions.API.Infrastructure.Http
{
    public class PagedResult
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
        public int TotalItems { get; set; }
    }
}
